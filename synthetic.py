import os as _os
import abc as _abc
import time as _time
import logging as _logging
import linecache as _cache
import tracemalloc as _trace


class Time(_abc.ABC):
    seconds = "time"
    milliseconds = "ms_time"


def display_snap_top(snapshot, key_type='lineno', limit=3):
    result = ""

    snapshot = snapshot.filter_traces((
        _trace.Filter(False, "<frozen importlib._bootstrap>"),
        _trace.Filter(False, "<unknown>"),
    ))

    top_stats = snapshot.statistics(key_type)

    result += f"Top {limit} lines\n"
    for index, stat in enumerate(top_stats[:limit], 1):
        frame = stat.traceback[0]

        filename = _os.sep.join(frame.filename.split(_os.sep)[-2:])
        result += f"#{index}: {filename}:{frame.lineno}: %.1f KiB\n" % (stat.size / 1024)
        line = _cache.getline(frame.filename, frame.lineno).strip()

        if line:
            result += f'\t{line}\n'

    other = top_stats[limit:]
    if other:
        size = sum(stat.size for stat in other)
        result += f"{len(other)} other: %.1f KiB\n" % (size / 1024)

    total = sum(stat.size for stat in top_stats)
    return result + "Total allocated size: %.1f KiB\n" % (total / 1024)


class Synthetic:
    def __init__(self):
        self._start = _time.time()
        self._snap = None
        self._end = None

        _trace.start()

    def __call__(self):
        self._snap = _trace.take_snapshot()
        _trace.clear_traces()  # Do not stop, just clear
        self._end = _time.time()

    @property
    def snap(self):
        return self._snap

    @property
    def time(self):
        """ Time taken in seconds """
        return round(self._end - self._start, 2)

    @property
    def ms_time(self):
        """ Time taken in milliseconds """
        return round((self._end - self._start) * 1000)

    def top(self, limit=3, output=print, time_in=Time.milliseconds):
        result = display_snap_top(self._snap, limit=limit)
        result += f"Time taken: {f'{self.ms_time} milliseconds' if time_in == Time.milliseconds else f'{self.time} seconds'}\n"

        output(result)


def test(logger=None, level='info', output=None, time_in=Time.milliseconds):
    """ Decorator for testing your function for memory allocation and time. It's synthetic test, ofc. """
    if output:
        """ Just do nothing """
    elif logger:
        output = _logging.getLogger(logger)

        if not hasattr(output, level):
            raise RuntimeError("No such log level")

        output = getattr(output, level)
    else:
        output = print

    def decorator(func):
        def wrapper(*args, **kwargs):
            testing = Synthetic()
            result = func(*args, **kwargs)
            testing()

            testing.top(output=output, time_in=time_in)

            return result

        return wrapper

    return decorator


def strict(function):
    import inspect

    signature = inspect.signature(function)

    assert signature.parameters.get("args") is None, "*args are not allowed in strict."
    assert signature.parameters.get("kwargs") is None, "**kwargs are not allowed in strict."

    for param in signature.parameters.values():
        assert param.annotation != inspect.Parameter.empty, f"Parameter type for [{param}] in '{function.__name__}' not set."

    def wrapper(*args, **kwargs):
        if len(args) > 0:
            for _ in zip(args, signature.parameters.values()):
                assert isinstance(_[0], _[1].annotation), f"Given value for parameter [{_[1]}] in '{function.__name__}' has wrong type {type(_[0])}."
        if len(kwargs) > 0:
            for name in kwargs:
                assert signature.parameters.get(name), f"Function '{function.__name__}' has no parameter called '{name}'"
                assert isinstance(kwargs.get(name), signature.parameters.get(name).annotation), f"Given value for parameter [{signature.parameters.get(name)}] in '{function.__name__}' has wrong type {type(kwargs.get(name))}."

        result = function(*args, **kwargs)
        assert isinstance(result, signature.return_annotation), f"Function '{function.__name__}' result is not {signature.return_annotation} type."
        return result

    wrapper.return_type = signature.return_annotation
    return wrapper
